<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liste de courses</title>
</head>
<body>
    
<h1>Ma liste de courses</h1>

<?php

// lecture du fichier et récuperation de la liste sous forme de tableau
if ( filesize("list.txt") == 0 ) {
    $products = [];
} else {
    $filename = "list.txt";
    $handle = fopen($filename, "r");
    $contents = fread($handle, filesize($filename));
    fclose($handle);

    $products = explode("\n", $contents);
}

// insertion d'un nouveau produit dans le tableau php
if ( isset($_POST['newProduct']) && !empty($_POST['newProduct']) ) {
    $products[] = $_POST['newProduct'];
}

// suppression d'un produit dans le tableau
if ( isset($_GET['id']) ) {
    unset($products[$_GET['id']]);
}

// sauvegarde du tableau dans le fichier list.txt
$fp = fopen('list.txt', 'w');
fwrite($fp, implode("\n", $products));
fclose($fp);

// affichage du tableau sous forme de liste HTML ul/li
echo '<ul>';
$i = 0;
foreach( $products AS $product ) {
    echo '<li>'.$product.' <a href="index.php?id='.$i.'">X</a></li>';
    $i++;
}
echo '</ul>';

// formulaire d'ajout d'un produit
?>
<form action="index.php" method="post">
    produit: <input type="text" name="newProduct">
    <input type="submit" value="ajouter">
</form>

</body>
</html>